package com.br.flickerfinder.photos.search.viewmodel

import com.br.flickerfinder.photos.search.viewmodel.utils.mockLogClass
import io.mockk.MockKAnnotations
import org.junit.Before
import org.junit.Test

class PhotoSearchViewModelTest {

    private lateinit var viewModel: PhotoSearchViewModel

    @Before
    fun setUp() {
        MockKAnnotations.init(this)
        viewModel = PhotoSearchViewModel()
        mockLogClass()
    }

    @Test
    fun onQueryTextChange() {
        val result = viewModel.onQueryTextChange("Car")
        assert(result != null)
    }
}