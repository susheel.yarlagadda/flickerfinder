package com.br.flickerfinder.common.adapter

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.br.flickerfinder.R
import com.br.flickerfinder.api.getImageUrl
import com.br.flickerfinder.databinding.ItemPhotoListBinding
import com.br.flickerfinder.photos.search.model.Photo
import com.bumptech.glide.Glide

typealias PhotoListItemClickCallback = ((Photo) -> Unit)

class PhotoListAdapter(
    private val photoListItemClickCallback: PhotoListItemClickCallback
) : RecyclerView.Adapter<PhotoListAdapter.PhotoListViewHolder>() {
    private var photosList: List<Photo> = listOf()

    inner class PhotoListViewHolder(val binding: ItemPhotoListBinding) :
        RecyclerView.ViewHolder(binding.root)

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): PhotoListViewHolder {
        return PhotoListViewHolder(
            ItemPhotoListBinding.inflate(
                LayoutInflater.from(parent.context),
                parent,
                false
            )
        )
    }

    override fun onBindViewHolder(holder: PhotoListViewHolder, position: Int) {
        val photoData = photosList[position]
        with(holder) {
            if (photoData.server != null && photoData.id != null && photoData.secret != null) {
                Glide.with(this.itemView)
                    .load(
                        getImageUrl(
                            photoData.farm.toString(),
                            photoData.server,
                            photoData.id,
                            photoData.secret
                        )
                    )
                    .centerCrop()
                    .placeholder(R.drawable.image_placeholder)
                    .into(binding.photoImage)
            }
            itemView.setOnClickListener { photoListItemClickCallback.invoke(photoData) }
            binding.imageTitle.text = photoData.title
        }
    }

    override fun getItemCount(): Int {
        return photosList.size
    }

    fun setUpdatedPhotoList(photosList: List<Photo>?) {
        this.photosList = photosList ?: listOf()
        notifyDataSetChanged()
    }
}