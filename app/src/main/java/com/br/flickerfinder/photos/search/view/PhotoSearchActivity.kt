package com.br.flickerfinder.photos.search.view

import android.os.Bundle
import android.view.View
import android.widget.SearchView
import androidx.appcompat.app.AppCompatActivity
import androidx.lifecycle.ViewModelProvider
import androidx.recyclerview.widget.LinearLayoutManager
import com.br.flickerfinder.R
import com.br.flickerfinder.common.adapter.PhotoListAdapter
import com.br.flickerfinder.databinding.ActivityPhotoSearchBinding
import com.br.flickerfinder.photos.search.model.Photo
import com.br.flickerfinder.photos.search.model.PhotosSearchResponse
import com.br.flickerfinder.photos.search.viewmodel.PhotoSearchViewModel
import com.br.flickerfinder.utils.UiUtils.displayGenericError

class PhotoSearchActivity : AppCompatActivity(), SearchView.OnQueryTextListener {
    private lateinit var binding: ActivityPhotoSearchBinding
    private lateinit var viewModel: PhotoSearchViewModel
    private lateinit var adapter: PhotoListAdapter

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityPhotoSearchBinding.inflate(layoutInflater)
        setContentView(binding.root)
        viewModel = ViewModelProvider(this).get(PhotoSearchViewModel::class.java)
        initPhotoListAdapter()

        binding.searchView.isSubmitButtonEnabled = true
        binding.searchView.setOnQueryTextListener(this)
    }

    private fun initPhotoListAdapter() {
        adapter = PhotoListAdapter { photo ->
            handlePhotoListItemClick(photo)
        }
        binding.photosRV.layoutManager =
            LinearLayoutManager(this)
        binding.photosRV.adapter = adapter
    }

    private fun handlePhotoListItemClick(photo: Photo) {
        launchPhotoDetailView(photo)
    }

    private fun getSearchResults(newText: String?) {
        viewModel.onQueryTextChange(newText)?.observe(this,
            { photosSearchResponse ->
                binding.progressBar.visibility = View.GONE
                if(photosSearchResponse.successResponse != null &&
                        photosSearchResponse.successResponse is PhotosSearchResponse){
                    photosSearchResponse.successResponse.photos?.photo.let { photoDataList ->
                        adapter.setUpdatedPhotoList(photoDataList)
                    }
                } else {
                    displayGenericError(this)
                }

            }
        )
    }

    private fun launchPhotoDetailView(photo: Photo) {
        supportFragmentManager.beginTransaction().add(
            R.id.fragmentContainer,
            PhotoDetailFragment.newInstance(photo)
        ).commit()
    }

    override fun onQueryTextSubmit(query: String?): Boolean {
        if(query.isNullOrEmpty().not()){
            binding.progressBar.visibility = View.VISIBLE
            getSearchResults(query)
        }
        return false
    }

    override fun onQueryTextChange(newText: String?): Boolean {
        if(newText.isNullOrEmpty()){
            adapter.setUpdatedPhotoList(null)
        }
        return false
    }
}