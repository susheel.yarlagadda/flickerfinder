package com.br.flickerfinder.photos.search.view

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import com.br.flickerfinder.R
import com.br.flickerfinder.utils.SystemUtils.hideKeyboard
import com.br.flickerfinder.api.getImageUrl
import com.br.flickerfinder.databinding.FragmentPhotoDetailBinding
import com.br.flickerfinder.photos.search.model.Photo
import com.bumptech.glide.Glide

class PhotoDetailFragment : Fragment() {

    private var binding: FragmentPhotoDetailBinding? = null

    companion object {
        private const val PHOTO_DATE = "PHOTO_DATE"
        fun newInstance(photo: Photo): PhotoDetailFragment{
            val fragment = PhotoDetailFragment()
            fragment.arguments = Bundle().apply {
                putParcelable(PHOTO_DATE, photo)
            }
            return fragment
        }
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        binding = FragmentPhotoDetailBinding.inflate(inflater, container, false)
        hideKeyboard(activity)

        val photoData: Photo? = arguments?.get(PHOTO_DATE) as? Photo
        if (photoData?.server != null && photoData.id != null && photoData.secret != null) {
            binding?.photoDetailImage?.let {
                Glide.with(this)
                    .load(
                        getImageUrl(
                            photoData.farm.toString(),
                            photoData.server,
                            photoData.id,
                            photoData.secret
                        )
                    )
                    .centerCrop()
                    .placeholder(R.drawable.image_placeholder)
                    .into(it)
            }
        }
        binding?.photoDetailImageClose?.setOnClickListener {
            activity?.supportFragmentManager?.beginTransaction()?.remove(this)?.commit()
        }
        return binding?.root
    }

    override fun onDestroy() {
        super.onDestroy()
        binding = null
    }
}