package com.br.flickerfinder.photos.search.model

import com.google.gson.annotations.SerializedName

data class Photos(
    val page: Int?,
    val pages: Int,
    @SerializedName("perpage") val perPage: Int,
    val photo: List<Photo>?,
    val total: Int
)