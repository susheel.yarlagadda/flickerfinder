package com.br.flickerfinder.photos.search.viewmodel

import android.util.Log
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.br.flickerfinder.api.model.NetworkResponseModel
import com.br.flickerfinder.photos.search.repository.PhotosDataRepository

class PhotoSearchViewModel : ViewModel() {

    private var filteredPhotoResponse: MutableLiveData<NetworkResponseModel>? = null

    fun onQueryTextChange(searchQuery: String?): LiveData<NetworkResponseModel>? {
        Log.e("PhotoSearchActivity", "onQueryTextChange $searchQuery")
        searchQuery?.let {
            filteredPhotoResponse = PhotosDataRepository.getSearchFilteredPhotoResponse(it)
        }
        return filteredPhotoResponse
    }
}