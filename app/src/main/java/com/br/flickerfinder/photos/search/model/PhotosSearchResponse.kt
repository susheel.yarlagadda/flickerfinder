package com.br.flickerfinder.photos.search.model

data class PhotosSearchResponse(
    val photos: Photos?,
    val stat: String?
)