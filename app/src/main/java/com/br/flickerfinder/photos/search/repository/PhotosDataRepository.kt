package com.br.flickerfinder.photos.search.repository

import android.util.Log
import androidx.lifecycle.MutableLiveData
import com.br.flickerfinder.api.RetrofitClient
import com.br.flickerfinder.api.model.NetworkErrorModel
import com.br.flickerfinder.api.model.NetworkResponseModel
import com.br.flickerfinder.photos.search.model.PhotosSearchResponse
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response


object PhotosDataRepository {

    var searchedFilteredPhotoResponse: MutableLiveData<PhotosSearchResponse> = MutableLiveData()
    var searchedFilteredPhotoResponseNew: MutableLiveData<NetworkResponseModel> = MutableLiveData()

    fun getSearchFilteredPhotoResponse(queryText: String): MutableLiveData<NetworkResponseModel> {
        val searchedFilteredPhotoCall = RetrofitClient.photoSearchAPI.fetchImages(queryText)
        searchedFilteredPhotoCall.enqueue(object : Callback<PhotosSearchResponse> {
            override fun onResponse(
                    call: Call<PhotosSearchResponse>,
                    response: Response<PhotosSearchResponse>
            ) {
                if (response.isSuccessful) {
                    searchedFilteredPhotoResponse.value = response.body()
                    searchedFilteredPhotoResponseNew.value = NetworkResponseModel(successResponse = response.body())
                } else {
                    searchedFilteredPhotoResponseNew.value = NetworkResponseModel(networkError =
                    NetworkErrorModel(errorMessage = response.message(), errorCode = response.code()))
                }
            }

            override fun onFailure(call: Call<PhotosSearchResponse>, t: Throwable) {
                searchedFilteredPhotoResponseNew.value = NetworkResponseModel(networkError =
                NetworkErrorModel(errorMessage = t.localizedMessage))
                Log.v("PhotosDataRepository : ", t.message.toString())
            }

        })

        return searchedFilteredPhotoResponseNew
    }
}