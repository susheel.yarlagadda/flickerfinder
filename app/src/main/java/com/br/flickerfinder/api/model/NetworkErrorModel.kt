package com.br.flickerfinder.api.model

data class NetworkErrorModel(
    val errorMessage: String?,
    val errorCode: Int? = null
)