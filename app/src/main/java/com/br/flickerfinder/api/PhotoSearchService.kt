package com.br.flickerfinder.api

import com.br.flickerfinder.photos.search.model.PhotosSearchResponse
import retrofit2.Call
import retrofit2.http.GET
import retrofit2.http.Query

interface PhotoSearchService {
    @GET("services/rest/?method=flickr.photos.search&api_key=1508443e49213ff84d566777dc211f2a&format=json&nojsoncallback=1")
    fun fetchImages(@Query("text") queryText: String?): Call<PhotosSearchResponse>
}