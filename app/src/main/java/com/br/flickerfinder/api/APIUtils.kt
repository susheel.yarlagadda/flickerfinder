package com.br.flickerfinder.api

internal fun getImageUrl(farmId: String, serverID: String, id: String, secret: String): String{
    return "https://farm$farmId.staticflickr.com/$serverID/${id}_$secret.jpg"
}