package com.br.flickerfinder.api.model

data class NetworkResponseModel(
    val successResponse: Any? = null,
    val networkError: NetworkErrorModel? = null
)