package com.br.flickerfinder.api

import okhttp3.OkHttpClient
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory


object RetrofitClient {

    private const val BASE_URL = "https://www.flickr.com/"
    private val retrofitClient: Retrofit.Builder by lazy {
        Retrofit.Builder()
            .baseUrl(BASE_URL)
            .client(OkHttpClient.Builder().build())
            .addConverterFactory(GsonConverterFactory.create())
    }

    val photoSearchAPI: PhotoSearchService by lazy {
        retrofitClient
            .build()
            .create(PhotoSearchService::class.java)
    }
}