package com.br.flickerfinder.utils;

import android.content.Context;

import androidx.appcompat.app.AlertDialog;

import com.br.flickerfinder.R;

/**
 * As per the requirement this class is kept in Java
 */
public class UiUtils {
    public static void displayGenericError(Context context) {
        new AlertDialog.Builder(context)
                .setTitle(context.getString(R.string.generic_error_title))
                .setMessage(context.getString(R.string.generic_error_message))
                .setPositiveButton(android.R.string.ok, (dialog, which) -> dialog.dismiss())
                .setIcon(android.R.drawable.ic_dialog_alert)
                .show();
    }
}
